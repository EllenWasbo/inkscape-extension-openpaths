# openPaths

This extension will open up closed paths by removing all z-commands from the selected paths.
My purpose: to save single line svg-fonts as otf or ttf fonts. These font formats require closed paths and will add a closing command to the glyphs. This closing can be removed by converting the text to paths, ungroup and then using this extension.

